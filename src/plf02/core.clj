(ns plf02.core)

(defn función-associative?-1
  [colección]
  (associative? colección))

(defn función-associative?-2
  [colección]
  (associative? colección))

(defn función-associative?-3
  [colección]
  (associative? colección))

(función-associative?-1 [1 2 3 4 5])
(función-associative?-2 '(9 2 3 4 5))
(función-associative?-3 #{\a \b \c \d})

(defn función-boolean?-1
  [x]
  (boolean? x))

(defn función-boolean?-2
  [x]
  (boolean? x))

(defn función-boolean?-3
  [x]
  (boolean? x))

(función-boolean?-1 true)
(función-boolean?-2 (< 10 2))
(función-boolean?-3 (= nil []))

(defn función-char?-1
  [c]
  (char? c))

(defn función-char?-2
  [c]
  (char? c))

(defn función-char?-3
  [c]
  (char? c))

(función-char?-1 \b)
(función-char?-2 "Hola")
(función-char?-3 10)

(defn función-coll?-1
  [colección]
  (coll? colección))

(defn función-coll?-2
  [colección]
  (coll? colección))

(defn función-coll?-3
  [colección]
  (coll? colección))

(función-coll?-1 {})
(función-coll?-2 nil)
(función-coll?-3 [])

(defn función-decimal?-1
  [número]
  (decimal? número))

(defn función-decimal?-2
  [número]
  (decimal? número))

(defn función-decimal?-3
  [número]
  (decimal? número))

(función-decimal?-1 1000000)
(función-decimal?-2 999999999999999999999999999)
(función-decimal?-3 4.7M)

(defn función-double?-1
  [número]
  (double? número))

(defn función-double?-2
  [número]
  (double? número))

(defn función-double?-3
  [número]
  (double? número))

(función-double?-1 3.4)
(función-double?-2 "2333")
(función-double?-3 4.3M)

(defn función-float?-1
 [número]
 (float? número))

(defn función-float?-2
 [número]
 (float? número))

(defn función-float?-3
 [número]
 (float? número))

(función-float?-1 \h)
(función-float?-2 0.4)
(función-float?-3 "Hola")

(defn función-ident?-1
  [x]
  (ident? x))

(defn función-ident?-2
  [x]
  (ident? x))

(defn función-ident?-3
  [x]
  (ident? x))

(función-ident?-1 :jorge)
(función-ident?-2 123)
(función-ident?-3 'abc)

(defn función-indexed?-1
  [colección]
  (indexed? colección))

(defn función-indexed?-2
  [colección]
  (indexed? colección))

(defn función-indexed?-3
  [colección]
  (indexed? colección))

(función-indexed?-1 [1 2 3 4])
(función-indexed?-2 '(1 2 3 4))
(función-indexed?-3 #{1 2 3 4})
(función-indexed?-1 {:a \A :b \B})

(defn función-int?-1
  [n]
  (int? n))

(defn función-int?-2
  [n]
  (int? n))

(defn función-int?-3
  [n]
  (int? n))

(función-int?-1 43)
(función-int?-2 0.45)
(función-int?-3 "Hola")

(defn función-integer?-1
  [in]
  (integer? in))

(defn función-integer?-2
  [in]
  (integer? in))

(defn función-integer?-3
  [in]
  (integer? in))

(función-integer?-1 10)
(función-integer?-2 10.0)
(función-integer?-3 45)

(defn función-keyword?-1
  [k]
  (keyword? k))

(defn función-keyword?-2
  [k]
  (keyword? k))

(defn función-keyword?-3
  [k]
  (keyword? k))

(función-keyword?-1 :key)
(función-keyword?-2 \h)
(función-keyword?-3 50)

(defn función-list?-1
 [x]
 (list? x))

(defn función-list?-2
 [x]
 (list? x))

(defn función-list?-3
 [x]
 (list? x))

(función-list?-1 '(1 2 3 4 5))
(función-list?-2 [])
(función-list?-3 23)

(defn función-map-entry?-1
  [x]
  (map-entry? (first x)))

(defn función-map-entry?-2
  [x]
  (map-entry? (first x)))

(defn función-map-entry?-3
  [x]
  (map-entry? (first x)))

(función-map-entry?-1 {:a \A :b \B})
(función-map-entry?-2 {1 2 3 4})
(función-map-entry?-3 [1 2 3 4])

(defn función-map?-1 
 [m]
 (map? m))

(defn función-map?-2
  [m]
  (map? m))

(defn función-map?-3
  [m]
  (map? m))

(función-map?-1 {:a 1 :b 2 :c 3})
(función-map?-2 '(1 2 3))
(función-map?-3 [1 2 3 4])

(defn función-nat-int?-1
  [n]
  (nat-int? n))

(defn función-nat-int?-2
  [n]
  (nat-int? n))

(defn función-nat-int?-3
  [n]
  (nat-int? n))

(función-nat-int?-1 0)
(función-nat-int?-2 1)
(función-nat-int?-3 -1)

(defn función-number?-1
  [a]
  (number? a))

(defn función-number?-2
  [a]
  (number? a))

(defn función-number?-3
  [a]
  (number? a))

(función-number?-1 \a)
(función-number?-2 3.4)
(función-number?-3 4.3M)

(defn función-pos-int?-1
  [b]
  (pos-int? b))

(defn función-pos-int?-2
  [b]
  (pos-int? b))

(defn función-pos-int?-3
  [b]
  (pos-int? b))

(función-pos-int?-1 123224)
(función-pos-int?-2 -2)
(función-pos-int?-3 -3545.31)

(defn función-ratio?-1
  [n]
  (ratio? n))

(defn función-ratio?-2
  [n]
  (ratio? n))

(defn función-ratio?-3
  [n]
  (ratio? n))

(función-ratio?-1 22/7)
(función-ratio?-2 2.2)
(función-ratio?-3 22)

(defn función-rational?-1
 [n]
 (rational? n))

(defn función-rational?-2
  [n]
  (rational? n))

(defn función-rational?-3
  [n]
  (rational? n))

(función-rational?-1 3/4)
(función-rational?-2 4)
(función-rational?-3 0.5)

(defn función-seq?-1
  [x]
  (seq? x))

(defn función-seq?-2
  [x]
  (seq? x))

(defn función-seq?-3
  [x]
  (seq? x))

(función-seq?-1 [1])
(función-seq?-2 '(1 2 3 4))
(función-seq?-3 #{\a \b \c})

(defn función-seqable?-1
  [x]
  (seqable? x))

(defn función-seqable?-2
  [x]
  (seqable? x))

(defn función-seqable?-3
  [x]
  (seqable? x))

(función-seqable?-1 [1 2 3 4])
(función-seqable?-2 "hola")
(función-seqable?-3 true)

(defn función-sequential?-1
  [colección]
  (sequential? colección))

(defn función-sequential?-2
  [colección]
  (sequential? colección))

(defn función-sequential?-3
  [colección]
  (sequential? colección))

(función-sequential?-1 [1 2 3 4 5])
(función-sequential?-2 [1 2 3 5 4])
(función-sequential?-3 {:a 3 :b 2 :c 1})

(defn función-set?-1 
  [x]
  (set? x))

(defn función-set?-2
  [x]
  (set? x))

(defn función-set?-3
  [x]
  (set? x))

(función-set?-1 #{1 2 3 4})
(función-set?-2 [1 2 3 4])
(función-set?-3 {:a 1 :b 2 :c 3})

(defn función-some?-1
  [s]
  (some? s))

(defn función-some?-2
  [s]
  (some? s))

(defn función-some?-3
  [s]
  (some? s))

(función-some?-1 nil)
(función-some?-2 true)
(función-some?-3 34)

(defn función-string?-1
  [x]
  (string? x))

(defn función-string?-2
  [x]
  (string? x))

(defn función-string?-3
  [x]
  (string? x))

(función-string?-1 "Jorge")
(función-string?-2 123)
(función-string?-3 \b)

(defn función-symbol?-1
  [s]
  (symbol? s))

(defn función-symbol?-2
  [s]
  (symbol? s))

(defn función-symbol?-3
  [s]
  (symbol? s))

(función-symbol?-1 'a)
(función-symbol?-2 \a)
(función-symbol?-3 "a")

(defn función-vector?-1
  [v]
  (vector? v))

(defn función-vector?-2
  [v]
  (vector? v))

(defn función-vector?-3
  [v]
  (vector? v))

(función-vector?-1 [1 2 3 4])
(función-vector?-2 '(1 2 3 4))
(función-vector?-3 #{1 2 3 4})

;Funciones de orden superior

(defn función-drop-1
  [n colección]
  (drop n colección))

(defn función-drop-2
  [n colección]
  (drop n colección))

(defn función-drop-3
  [n colección]
  (drop n colección))

(función-drop-1 2 [1 2 3 4 5])
(función-drop-2 5 [1 2 3])
(función-drop-3 7 [5 4 2 3 52 3 4 5 2])

(defn función-drop-last-1
  [colección]
  (drop-last colección))

(defn función-drop-last-2
  [n colección]
  (drop-last n colección))

(defn función-drop-last-3
  [n colección]
  (drop-last n colección))

(función-drop-last-1 [1 2 3 4 5])
(función-drop-last-2 2 [2 4 1 3 5])
(función-drop-last-3 5 [1 2 3])

(defn función-drop-while-1
  [pred colección]
  (drop-while pred colección))

(defn función-drop-while-2
  [pred colección]
  (drop-while pred colección))

(defn función-drop-while-3
  [pred colección]
  (drop-while pred colección))

(función-drop-while-1 neg? [-1 -2 -3 -4 5 6 7])
(función-drop-while-2 pos? [1 2 3 4 5 6 -2 -4 -5])
(función-drop-while-3 #(> 5 %) [1 2 3 4 5 6 7 8 9 10])

(defn función-every?-1
  [pred colección]
  (every? pred colección))

(defn función-every?-2
  [pred colección]
  (every? pred colección))

(defn función-every?-3
  [pred colección]
  (every? pred colección))

(función-every?-1 even? [2 4 6 8 10])
(función-every?-2 pos? [1 2 3 4 5 -1])
(función-every?-3 neg? '(-1 -2 -3 -4))

(defn función-filterv-1
  [pred colección]
  (filterv pred colección))

(defn función-filterv-2
  [pred colección]
  (filterv pred colección))

(defn función-filterv-3
  [pred colección]
  (filterv pred colección))

(función-filterv-1 even? [1 2 3 4 5 6 7 8 9 10])
(función-every?-2 pos? [1 2 3 4 5 -1 -2 -3 -4 -5])
(función-filterv-3 number? '(a \b 1 2 "3"))

(defn función-group-by-1
  [pred colección]
  (group-by pred colección))

(defn función-group-by-2
  [pred colección]
  (group-by pred colección))

(defn función-group-by-3
  [pred colección]
  (group-by pred colección))

(función-group-by-1 count ["Hola" "Mundo" "Clojure"])
(función-group-by-2 number? '(\a \b 1 2 3 :s "s"))
(función-group-by-3 even? [1 2 3 4 5 6 7 8 9 10])

(defn función-iterate-1
  [f x]
  (iterate f x))

(defn función-iterate-2
  [f x]
  (iterate f x))

(defn función-iterate-3
  [f x]
  (iterate f x))

(función-iterate-1 inc 5)
(función-iterate-2 dec 10)
(función-iterate-3 (fn [x] (* 2 x)) 10)

(defn función-keep-1
  [pred colección]
  (keep pred colección))

(defn función-keep-2
  [pred colección]
  (keep pred colección))

(defn función-keep-3
  [pred colección]
  (keep pred colección))

(función-keep-1 odd? [1 2 3 4 5 6 7 8 9 10])
(función-keep-2 even? [1 2 3 4 5 6 7 8 9 10])
(función-keep-3 pos? [1 2 3 -3 -5 -6 -7 -3 10])

(defn función-keep-indexed-1
  [f colección]
  (keep-indexed f colección))

(defn función-keep-indexed-2
  [f colección]
  (keep-indexed f colección))

(defn función-keep-indexed-3
  [f colección]
  (keep-indexed f colección))

(función-keep-indexed-1 #(if (even? %1) %2) [\a \e \i \o \u])
(función-keep-indexed-2 #(if (odd? %1) %2) [:a :b :c :d :e])
(función-keep-indexed-3 (fn [idx v] (if (pos? v) idx)) [-9 0 29 -7 45 3 -8])

(defn función-map-indexed-1
  [f colección]
  (map-indexed f colección))

(defn función-map-indexed-2
  [f colección]
  (map-indexed f colección))

(defn función-map-indexed-3
  [f colección]
  (map-indexed f colección))

(función-map-indexed-1 vector "Jorge")
(función-map-indexed-2 hash-map "Clojure")
(función-map-indexed-3 #(when (< % 2) %2) [\a \e \i \o \u])

(defn función-mapcat-1
  [f colección]
  (mapcat f colección))

(defn función-mapcat-2
  [f colección]
  (mapcat f colección))

(defn función-mapcat-3
  [f colección]
  (mapcat f colección))

(función-mapcat-1 reverse [[3 2 1] [6 5 4] [9 8 7]])
(función-mapcat-2 (fn [n] [(- n 1) (+ n 1)]) [1 2 3 4 5 6])
(función-mapcat-3 (fn [n] [(* n 2) n (* n 4)]) [1 2 3 4 5 6])

(defn función-mapv-1
  [f col]
  (mapv f col))

(defn función-mapv-2
  [f col1 col2]
  (mapv f col1 col2))

(defn función-mapv-3
  [f col1 col2 col3]
  (mapv f col1 col2 col3))

(función-mapv-1 (fn [n] (* 2 n)) [1 2 3])
(función-mapv-2 * [1 2 3] [4 5 6])
(función-mapv-3 + [1 2 3] [4 5 6] [7 8 9])

(defn función-merge-with-1
  [f m1 m2]
  (merge-with f m1 m2))

(defn función-merge-with-2
  [f m1 m2 m3]
  (merge-with f m1 m2 m3))

(defn función-merge-with-3
  [f m1 m2 m3 m4]
  (merge-with f m1 m2 m3 m4))

(función-merge-with-1 + {:a 10 :b 20} {:a 100 :b 200})
(función-merge-with-2 * {:a 2 :b 3 :c 4} {:a 6 :b 9 :c 9} {:a 3 :b 5 :c 9})
(función-merge-with-3 / {:a 2 :b 4} {:a 6 :b 3} {:a 8 :b 9} {:a 2 :b 6})

(defn función-not-any?-1
  [pred colección]
  (not-any? pred colección))

(defn función-not-any?-2
  [pred colección]
  (not-any? pred colección))

(defn función-not-any?-3
  [pred colección]
  (not-any? pred colección))

(función-not-any?-1 odd? [2 4 6 8 10])
(función-not-any?-2 even? [1 3 5 7 9])
(función-not-any?-3 nil? [nil true true 2])

(defn función-not-every?-1
  [pred colección]
  (not-every? pred colección))

(defn función-not-every?-2
  [pred colección]
  (not-every? pred colección))

(defn función-not-every?-3
  [pred colección]
  (not-every? pred colección))

(función-not-every?-1 odd? [1 2 3 4 5])
(función-not-every?-2 even? [9 8 7 6 5])
(función-not-every?-3 pos? [1 -2 3 -4 -5 6])

(defn función-partition-by-1
  [pred colección]
  (partition-by pred colección))

(defn función-partition-by-2
  [pred colección]
  (partition-by pred colección))

(defn función-partition-by-3
  [pred colección]
  (partition-by pred colección))

(función-partition-by-1 odd? [1 2 3 4 5 6 7])
(función-partition-by-2 neg? [-3 -2 -1 0 1 2 3])
(función-partition-by-3 #(= 5 %) [1 2 3 5 6 7 8 5 9 10])

(defn función-reduce-kv-1
  [f i col]
  (reduce-kv f i col))

(defn función-reduce-kv-2
  [f i col]
  (reduce-kv f i col))

(defn función-reduce-kv-3
  [f i col]
  (reduce-kv f i col))

(función-reduce-kv-1 (fn [s k v] (if (even? k) (+ s v) s)) 0 [0 1 2 3 4 5 6 7 8 9])
(función-reduce-kv-2 + 0 [1 2 3 4 5])
(función-reduce-kv-3 - 0 [2 3 4 5 6 7 5 4])

(defn función-remove-1
 [pred colección]
 (remove pred colección))

(defn función-remove-2
  [pred colección]
  (remove pred colección))

(defn función-remove-3
  [pred colección]
  (remove pred colección))

(función-remove-1 odd? [1 2 3 4 5 6 7])
(función-remove-2 nil? [1 nil 2 nil 3])
(función-remove-3 pos? [1 -2 -3 4 5 6 7])

(defn función-reverse-1
  [colección]
  (reverse colección))

(defn función-reverse-2
  [colección]
  (reverse colección))

(defn función-reverse-3
  [colección]
  (reverse colección))

(función-reverse-1 [5 4 3 2 1])
(función-reverse-2 "PROGRAMACIÓN")
(función-reverse-3 '(1 2 3 4 5))

(defn función-some-1
  [pred colección]
  (some pred colección))

(defn función-some-2
  [pred colección]
  (some pred colección))

(defn función-some-3
  [pred colección]
  (some pred colección))

(función-some-1 even? [1 2 3 4 5])
(función-some-2 even? [1 3 5 7 9])
(función-some-3 true? [false false false true])

(defn función-sort-by-1
  [k colección]
  (sort-by k colección))

(defn función-sort-by-2
  [k colección]
  (sort-by k colección))

(defn función-sort-by-3
  [k comp colección]
  (sort-by k comp colección))

(función-sort-by-1 count ["programación" "clojure" "funcional"])
(función-sort-by-2 first [[7 8] [3 4] [2 5] [5 2] [1 2]])
(función-sort-by-3 first > [[7 8] [3 4] [2 5] [5 2] [1 2]])

(defn función-split-with-1
  [pred colección]
  (split-with pred colección))

(defn función-split-with-2
  [pred colección]
  (split-with pred colección))

(defn función-split-with-3
  [pred colección]
  (split-with pred colección))

(función-split-with-1 #(> 4 %) [1 2 3 4 5 6 7])
(función-split-with-2 odd? [1 3 5 6 7 9])
(función-split-with-3 neg? [-3 -2 -1 0 1 2 3 4])


(defn función-take-1
  [n colección]
  (take n colección))

(defn función-take-2
  [n colección]
  (take n colección))

(defn función-take-3
  [n colección]
  (take n colección))

(función-take-1 2 [1 2 3 4 5])
(función-take-2 10 (range 100))
(función-take-3 5 (range 3))

(defn función-take-last-1
  [n colección]
  (take-last n colección))

(defn función-take-last-2
  [n colección]
  (take-last n colección))

(defn función-take-last-3
  [n colección]
  (take-last n colección))

(función-take-last-1 2 [1 2 3 4 5 6 7 8])
(función-take-last-2 10 (range 5 50))
(función-take-last-3 0 [1])

(defn función-take-nth-1
  [n colección]
  (take-nth n colección))

(defn función-take-nth-2
  [n colección]
  (take-nth n colección))

(defn función-take-nth-3
  [n colección]
  (take-nth n colección))

(función-take-nth-1 2 (range 10))
(función-take-nth-2 -1 [1 2 3 4 5 6 7])
(función-take-nth-3 5 [1 2 3 4 5 6 7 8 9 10])

(defn función-take-while-1
  [pred colección]
  (take-while pred colección))

(defn función-take-while-2
  [pred colección]
  (take-while pred colección))

(defn función-take-while-3
  [pred colección]
  (take-while pred colección))

(función-take-while-1 neg? [-3 -2 -1 0 1 2 3 4])
(función-take-while-2 (fn [x] (< x 10)) (range 100))
(función-take-while-3 neg? [])

(defn función-update-1
  [m k f]
  (update m k f))

(defn función-update-2
  [m k f]
  (update m k f))

(defn función-update-3
  [m k f]
  (update m k f))

(función-update-1 {:nombre "Jorge" :edad 21} :edad inc)
(función-update-2 [1 2 3] 0 dec)
(función-update-3 [2 5 2 5 8 9] 4 #(if (> % 5) (inc %) (dec %)))

(defn función-update-in-1
  [m ks f]
  (update-in m ks f))

(defn función-update-in-2
  [m ks f]
  (update-in m ks f))

(defn función-update-in-3
  [m ks f arg1 arg2]
  (update-in m ks f arg1 arg2))

(def usuarios [{:nombre "Jorge" :edad 26}  
               {:nombre "Luis" :edad 23}])

(función-update-in-1 usuarios [1 :edad] inc)
(función-update-in-2 usuarios [0 :edad] dec)
(función-update-in-3 {:a 3 :b 6} [:a] / 4 5)
